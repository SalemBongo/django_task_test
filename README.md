django_task_test
---
START:
* pip install -r requirements.txt

API endpoints:
* auth/
* accounts/profile/
* questions/
* answers/
* answers/{pk}