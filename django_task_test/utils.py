from threading import Thread


def thread(func):
    """Thread-decorator."""

    def wrapper(*args, **kwargs):
        my_thread = Thread(target=func, args=args, kwargs=kwargs)
        my_thread.start()
        return wrapper
