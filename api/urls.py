from django.urls import path, include
from django.shortcuts import HttpResponsePermanentRedirect
from rest_framework.urlpatterns import format_suffix_patterns

from api import views


urlpatterns = [
    path('auth/', include('rest_framework.urls'),
         name='auth'),
    path('', lambda r: HttpResponsePermanentRedirect('questions/'),
         name='homepage'),
    path('accounts/profile/', views.ProfileListView.as_view(),
         name='profile'),
    path('questions/', views.QuestionListCreateView.as_view(),
         name='questions'),
    path('answers/', views.AnswerCreateView.as_view(),
         name='answers'),
    path('answers/<int:pk>/', views.AnswerUpdateView.as_view(),
         name='answer'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
