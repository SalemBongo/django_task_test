from django_filters.rest_framework import FilterSet, CharFilter

from api.models import Question


class QuestionSearchFilter(FilterSet):
    """Filtering by 'title' (str). Entry of an element in the fields."""

    title = CharFilter(field_name='title', lookup_expr='icontains')

    class Meta:
        model = Question
        fields = ['title']
