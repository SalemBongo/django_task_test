from django.contrib.admin import ModelAdmin, register

from api.models import Profile, Answer, Question


@register(Profile)
class ProfileAdmin(ModelAdmin):

    list_display = [field.name for field in Profile._meta.fields]

    class Meta:
        model = Profile


@register(Answer)
class AnswerAdmin(ModelAdmin):

    list_display = [field.name for field in Answer._meta.fields]

    class Meta:
        model = Answer


@register(Question)
class QuestionAdmin(ModelAdmin):

    list_display = [field.name for field in Question._meta.fields]

    class Meta:
        model = Question
