from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, ValidationError
from datetime import datetime, timedelta

from api.validators import ExcludeValueValidator


class Profile(models.Model):
    """Profile model related with default Django-model of User."""

    user = models.OneToOneField(User,
                                on_delete=models.CASCADE)
    answered_question = models.IntegerField(null=True)
    unanswered_question = models.IntegerField(null=True)

    def __str__(self):
        return 'Profile ID: {0}'.format(self.user.id)


class Question(models.Model):
    """Question model."""

    title = models.CharField(max_length=255,
                             blank=False,
                             unique=True)
    end_time = models.DateTimeField(default=datetime.now())
    real_answer = models.PositiveSmallIntegerField(null=True,
                                                   blank=True)

    def can_answer(self):  # check can_answer
        return self.end_time >= datetime.now()

    def save(self, **kwargs):  # auto-duration, for example
        self.end_time += timedelta(hours=24)
        super(Question, self).save()

    def __str__(self):
        return 'Title: {0} | RealAnswer: {1}'.format(
            self.title, self.real_answer)


class Answer(models.Model):
    """Answer model."""

    user = models.ForeignKey(Profile,
                             on_delete=models.SET_NULL,
                             null=True)
    question = models.ForeignKey(Question,
                                 on_delete=models.SET_NULL,
                                 null=True)
    answer = models.PositiveSmallIntegerField(validators=[
        MaxValueValidator(100), ExcludeValueValidator(50)])
    timestamp = models.DateTimeField(default=datetime.now())

    class Meta:
        unique_together = ('user', 'question')

    def can_edit(self):  # check can_edit
        return self.timestamp <= (datetime.now() + timedelta(hours=1))

    def save(self, **kwargs):
        if not self.question.can_answer:
            raise ValidationError('Time of poll is over.')
        elif not self.can_edit:
            raise ValidationError('Time for edit answer is over.')
        super(Answer, self).save()

    def __str__(self):
        return 'Profile: {0} | Answer: {1} | CanEdit: {2}'.format(
            self.user_id, self.answer, self.can_edit)
