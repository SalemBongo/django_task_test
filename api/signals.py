from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save

from api.models import Profile, Answer, Question
from django_task_test.utils import thread


@thread
@receiver(post_save, sender=User,
          dispatch_uid='profile_create_signal')
def create_user_profile(sender, instance, created, **kwargs):
    """
    Signal for create Profile through default Django-model of User.
    Run in thread.
    """

    if created:
        Profile.objects.create(user=instance)


@thread
@receiver(post_save, sender=User,
          dispatch_uid='profile_update_signal')
def save_user_profile(sender, instance, **kwargs):
    """
    Signal for update Profile through default Django-model of User.
    Run in thread.
    """

    instance.profile.save()


@thread
@receiver(post_save, sender=Answer,
          dispatch_uid='profile_count_answers_signal')
def count_answers(sender, instance, created, **kwargs):
    """
    Signal for update statistics in Profile after Answer creation.
    Run in thread.
    """

    if created:
        user = User.objects.get(
            pk=instance.user_id).select_related('profile')
        user.profile.answered_questions = Answer.objects.filter(
            user_id=instance.user_id).all().count()
        user.profile.unanswered_questions = (Question.objects.all().count()
                                             - user.profile.answered_questions)
        user.profile.save(update_fields=['answered_questions',
                                         'unanswered_questions'])
