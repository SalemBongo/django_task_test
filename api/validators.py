from django.core.validators import BaseValidator


class ExcludeValueValidator(BaseValidator):
    """Not 50 value validator."""

    message = 'Please use values from 0 to 100 excluding 50.'

    def compare(self, not_valid_value, entered_value):
        return not_valid_value == entered_value
