from rest_framework.permissions import BasePermission


class IsAdminForPostMethods(BasePermission):
    """GET, POST methods permissions for is_staff."""

    def has_permission(self, request, view):
        if request.method in ['GET', 'POST'] and request.user.is_staff:
            return True
        elif request.method == 'GET':
            return True
