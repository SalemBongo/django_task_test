from rest_framework.serializers import ModelSerializer

from api.models import Question, Answer, Profile


class QuestionSerializer(ModelSerializer):
    """Serializer for Question instance."""

    class Meta:
        model = Question
        fields = ('id', 'title', 'end_time', 'real_answer', 'can_answer',)
        read_only_fields = ('can_answer',)


class AnswerSerializer(ModelSerializer):
    """Serializer for Answer instance."""

    class Meta:
        model = Answer
        fields = ('id', 'question', 'answer', 'can_edit',)
        read_only_fields = ('can_edit',)


class ProfileSerializer(ModelSerializer):
    """Serializer for User instance."""
    class Meta:
        model = Profile
        fields = '__all__'
