from rest_framework.generics import (ListAPIView, ListCreateAPIView,
                                     RetrieveUpdateAPIView)
from django_filters.rest_framework import DjangoFilterBackend

from api.models import Question, Answer, Profile
from api.serializers import (QuestionSerializer, AnswerSerializer,
                             ProfileSerializer)
from api.permissions import IsAdminForPostMethods
from api.filters import QuestionSearchFilter


class ProfileListView(ListAPIView):
    """Get Profile, statistics of User."""

    serializer_class = ProfileSerializer

    def get_queryset(self):
        return Profile.objects.filter(user_id=self.request.user.id)


class QuestionListCreateView(ListCreateAPIView):
    """Get list of Questions."""

    serializer_class = QuestionSerializer
    permission_classes = (IsAdminForPostMethods,)
    filter_backends = (DjangoFilterBackend,)
    filter_class = QuestionSearchFilter  # api.filters

    def get_queryset(self):
        """
        Filtering by query param.
        :answered / :unanswered (bool)
        :active / :inactive (bool)
        """

        queryset = Question.objects.all()
        if self.request.query_params.get('answered'):
            queryset = queryset.filter(
                answer__user_id=self.request.user.id).all()
        elif self.request.query_params.get('unanswered'):
            queryset = queryset.exclude(
                answer__user_id=self.request.user.id).all()
        elif self.request.query_params.get('active'):
            queryset = queryset.filter(
                can_answer=True).all()
        elif self.request.query_params.get('inactive'):
            queryset = queryset.exclude(
                can_answer=True).all()
        return queryset


class AnswerCreateView(ListCreateAPIView):
    """Get or Create Answer."""

    serializer_class = AnswerSerializer

    def get_queryset(self):
        return Answer.objects.filter(user_id=self.request.user.id)

    def perform_create(self, serializer):
        serializer.save(user_id=self.request.user.id)


class AnswerUpdateView(RetrieveUpdateAPIView):
    """Update Answer."""

    serializer_class = AnswerSerializer
    lookup_field = 'pk'

    def get_queryset(self):
        return Answer.objects.filter(user_id=self.request.user.id)

    def perform_create(self, serializer):
        serializer.save(user_id=self.request.user.id)
